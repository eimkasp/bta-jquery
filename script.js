var prisijungti = $("#prisijungti");

$("[href='https://google.lt']").on('click', function() {
    alert("test");
});

$(".btn").on('click',function() {
    
    // this grazina elementa kuris iskviete musu event listener
    console.log( $(this));

    // $(this).attr('data-elementas');

    if(meluoji == false) {
        if($(this).data('elementas')) {
            var elementasKuriRodysiu = $(this).data('elementas');
            $(elementasKuriRodysiu).fadeIn();
        }
    } else {
        alert("baik meluot");
    }

    


});


// Pvz su boostrap modal
$("#registruotis").click(function() {
    $('#registracija').modal('show');
});


// Event listeneris fiksuojantis klaviaturos paspaudimus
// event.keyCode visada grazina paspausto klaviso koda
// Dazniausiai toki event listener turetume priskirti window kintamajam
$(window).on('keyup', function(event) {
    // grazinamas paspausto mygtuko keycode
    console.log(event.keyCode);
    // Escape mygtuko kodas yra 27
    if(event.keyCode == 27) {
        console.log('paspaudei escape');
        $(".my-modal").fadeOut();
    }
});


$(".my-modal").on('click', function(event) {

    var jQueryTarget = $(event.target);
    
    // Patikrinu ar paspaudimas my-modal viduje, buvo spaustas
    // vidinis elementas ar paspaudziau ant fono
    if($(event.target).hasClass('my-modal')) {
       $(".my-modal").fadeOut();
    }
});


// jQuery event listener
// prisijungti.on('click',function() {
//     $(".my-modal").fadeIn();
// });

$(".close-modal").on('click', function() {
    // $(this);
    $(this).parent().parent().fadeOut();
});